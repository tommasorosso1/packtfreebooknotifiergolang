package main

import (
	"net/http"
	"github.com/PuerkitoBio/goquery"
	"fmt"
	"strings"
	"golang.org/x/net/html"
	"log"
)

func main() {
	url := "https://www.packtpub.com/packt/offers/free-learning"

	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	htmlNode, err := html.Parse(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	title, err := extractTitleFromPage(htmlNode)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(title)

}

func extractTitleFromPage(node *html.Node) (string, error) {
	doc := goquery.NewDocumentFromNode(node)

	title := strings.TrimSpace(doc.Find(".dotd-title").First().Find("h2").Text())
	if title != "" {
		return title, nil
	}
	return "", fmt.Errorf("%s", "Title not found, inside html check the selector")
}
